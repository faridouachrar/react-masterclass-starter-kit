const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HotModuleReplacementPlugin = require("react-hot-loader");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const devPlugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, "./public/index.html")
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || "development")
    }
  })
];
const prodPlugins = [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.optimize.AggressiveMergingPlugin(),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  new UglifyJSPlugin(),
  new webpack.DefinePlugin({
    "process.env.NODE_ENV": JSON.stringify("production")
  })
];

const plugins =
  process.env.NODE_ENV == "development" ? devPlugins : prodPlugins;

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: path.resolve(__dirname, "index.js"),
  output: {
    path: path.resolve(__dirname, "public/js"),
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: ["babel-loader"]
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.ttf$|\.eot$|\.svg$/,
        use: "file-loader?name=[name].[ext]?[hash]"
      }
    ]
  },
  plugins,
  devServer:
    process.env.NODE_ENV == "development"
      ? {
          host: "localhost",
          port: 3000,
          historyApiFallback: true,
          open: true,
          hot: true
        }
      : {}
};
