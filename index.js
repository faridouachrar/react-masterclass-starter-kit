import React, {Component} from 'react';
import {render} from 'react-dom';
import "./src/styles/index.scss";

class App extends Component {
  render() {
    return (
      <div>
        <h3>Masterclass starter kit</h3>        
      </div>
    )
  }
}

render(<App />, document.getElementById("root"));